%
% ------------------------------------------------------
% Methods for Optimal Resource Allocation in LTE Uplink
% Code for Method 2 - SDP and bintprog
% Author: Naveen Mysore Balasubramanya
%------------------------------------------------------
%
clc;
clear all;
close all;

randStream = RandStream('mt19937ar','Seed', 2);

% M = Number of Users
% N = Number of PRBs

sMin = -5; %SNRmin = 0dB
sMax = 20; %SNRmax = 15dB

% Begin with a Uniform Distribution of Total Power per User
Pmin = 50; % Min power is 100mW
Pmax = 100; % Max power is 200mW


for M = [2 5 10]
    for N = [2*M 2*M+2 2*M+4] 
        
        % w is the QoS weights per User
        % Begin with a same weights per User
        w = ones(M,1);

        % In the matrices below, each row is a user, each column is a PRB
        P = Pmin + (Pmax - Pmin)*rand(randStream, M, 1);

        % s is the maximum SINR constraint per RB per User 
        s = 10^(sMin/10) + (10^(sMax/10) - 10^(sMin/10))*rand(randStream, M, N);


        % Power levels correspodning to s
        % Note that maximum power of 20mW results in maximum SNR
        t = s;
        for i = 1:M
            t(i,:) = s(i,:)*20/(10^(sMax/10)); 
        end

        % Compute adjacency matrix B
        colNum = 2;
        for k = 1:N
            for l = 0:N-k
                B(:,colNum) = [zeros(l,1); ones(k,1); zeros(N-l-k,1)];
                colNum = colNum + 1;
            end
        end

        dim = size(B);
        eta = sum(B);
        
        % Compute reward vector c
        k = 1;
        for i = 1:M
            for j = 1:dim(2)
                PallocMax = t(i,:)'.*B(:,j);
                Pavg = P(i)/eta(j)*ones(N,1);
                Palloc = min(Pavg, PallocMax);
                storePalloc(i,j,:) = Palloc';
                c(k) = w(i)*sum(log2(1+Palloc));
                k = k + 1;
            end
        end

        % Compute constraint matrix A
        row_num = 1;
        for k = 1:M    
            Atemp(row_num,:) = [zeros(1,(k-1)*dim(2)) ones(1, dim(2)) zeros(1, (M-k)*dim(2))];
            row_num = row_num + 1;
        end
        A = [repmat(B,1, M); Atemp];
        [m, n] = size(A);

        % -------------- Binary Integer programming ----------------------
        options = optimset('Display','off');
        [y, objVal] = bintprog(-c,[],[],A,ones(m,1),[],options);

        col_idx = find(y == 1);
        col_idx = mod(col_idx-1,dim(2)) + 1;
 
        for i = 1:M
            x(i,:) = B(:,col_idx(i))';
            p(i,:) = storePalloc(i,col_idx(i),:);
        end

        disp(['bintprog Throughput = ' num2str(-1*objVal)]);
        storeFinalThroughput_bintprog(M,N) = -1*objVal;
        % ---------------------------------------------------------------
        
        
        % ----------------- SDP ------------------------------------------
        if(M <=5 && N <= 14)
            C = diag([0 c]);

            cvx_begin quiet

            cvx_precision low
            

            variable Z(n+1,n+1) symmetric;

            maximize( trace(C*Z) )
            subject to

                for i = 1:m
                    trace(diag([0 A(i,:)])*Z)== 1;
                end

                diag(Z)-Z(1,1:n+1)'== zeros(n+1,1);
                Z(1,1) == 1;
                Z == semidefinite(n+1);

            cvx_end

            Y = Z(1,2:1:n+1);
            col_idx = find(Y > 0.999);
            col_idx = mod(col_idx-1,dim(2))+1;

            for i = 1:M
                x(i,:) = B(:,col_idx(i))';
                p(i,:) = storePalloc(i,col_idx(i),:);
            end

            disp(['SDP Throughput = ' num2str(cvx_optval)]);
            storeFinalThroughput_SDP(M,N) = cvx_optval;
        end
        % ---------------------------------------------------------------
        
        clear B A Atemp Palloc storePalloc x p;
    end
end